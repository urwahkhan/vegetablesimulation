/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
abstract class Vegetable {
    String color;
    double size;
    abstract boolean isRipe();
    abstract String p();
    public Vegetable(String color,double size){
        this.color=color;
        this.size=size;
        System.out.println("Color: "+color+" Size: "+size);
    }
    
    public String getColor(){
        return this.color;
    }
        
    public double getSize(){
        return this.size;
    }
   
}
